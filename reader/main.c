/*=============================================================================================
|
|       Writer
|       Author:         Tiago Sousa Rocha
|       Description:    This is a software which read in Virtual Memory Space.
|
===============================================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include "../tipos.h"

#define SW_VERSION "1.00-r00"
#define SUCCESS 0
#define FAILED  1
#define MODE 0x6640
#define SIZE 1024
#define COUNT_FOR 100
#define SLEEP_TIME 5


int main (void) 
{
    /* File Descriptor */
    int fd=-1;
    /* Memory Mapped*/
    int *mem=NULL;
    /* Indice */
    int idx = 0;
    /* Buffer */
    unsigned char buffer[10]={};
    /* */
    TData data;
    /**/
    TData *pdata;

    fprintf(stdout, "\n==> Debug:\tIniciando o software Reader. [version: %s]", SW_VERSION);

    /* Initialize */
    memset(&data, 0, sizeof(TData));

    // Abre o arquivo para apenas leitura
    fd=open("/tmp/datafile", O_RDWR);
    if(fd < 0)
    {
        perror("Não pôde abrir o arquivo datafile!");
        return FAILED;
    }
    fprintf(stdout, "\n==> Debug:\tArquivo (%u) /tmp/datafile foi aberto com sucesso!", fd);
    
    pdata=(TData *)mmap(NULL, getpagesize(), PROT_WRITE, MAP_SHARED, fd, 0);
    if(pdata == MAP_FAILED)
    {
        perror("Não pôde mapear o endereço de memória do arquivo!");
        return FAILED;
    }
    fprintf(stdout, "\n==> Debug:\tMapeamento virtual foi criado com sucesso!\n");
    
    for (idx=0; idx<COUNT_FOR; idx++)
    {
        data.index = pdata->index;
        data.value = pdata->value;
        fprintf(stdout, "\n==> Debug:\tReader leu: [indice, valor] [%d, %d] \n", data.index, data.value);
        fprintf(stdout, "\n==> Debug:\tReader entra em Sleep por %d segundos. \n", SLEEP_TIME);
        
        sleep(SLEEP_TIME);
    }
    //
    munmap(mem, getpagesize());

    // Fecha o arquivo
    close(fd);

    fprintf(stdout, "\n==> Debug:\tFinalizando o software Reader.\n");
    return SUCCESS;
}