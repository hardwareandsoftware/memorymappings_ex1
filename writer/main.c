/*=============================================================================================
|
|       Writer
|       Author:         Tiago Sousa Rocha
|       Description:    This is a software which writes in Shamerd Memory in Virtual Space 
|                   Memory. It is writes 1 to 100, step-by-step each 30 seconds.
|
===============================================================================================*/
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include "../tipos.h"

#define SW_VERSION "1.00-r00"
#define SUCCESS 0
#define FAILED  1
#define MODE 0x6640
#define SIZE 1024
#define COUNT_FOR 100
#define SLEEP_TIME 30
#define RWX_UGO 0x100600 | (S_IRWXU | S_IRWXG | S_IRWXO)
#define RAND_MAX 300

int main (void) 
{
    /* File Descriptor */
    int fd=-1;
    /* Memory Mapped*/
    int *mem=NULL;
    /* Indice */
    int idx = 0;
    /*  */
    struct stat sb;
    /* */
    mode_t mode;
    /* Buffer */
    unsigned char buffer[10]={};
    /* */
    TData data;

    fprintf(stdout, "\n==> Debug:\tIniciando o software Writer. [version: %s]", SW_VERSION);

    /* Initialize */
    memset(&data, 0, sizeof(TData));

    if (stat("/tmp/datafile", &sb) != 0)
    {
        fprintf(stdout, "\n==> Debug:\tStats do Arquivo falhou!");
    }
    mode = sb.st_mode & RWX_UGO;

    // Abre o arquivo para escrita
    fd=open("/tmp/datafile", O_CREAT|O_RDWR|O_TRUNC, mode);
    if(fd < 0)
    {
        perror("Não pôde abrir o arquivo datafile!");
        return FAILED;
    }
    fprintf(stdout, "\n==> Debug:\tArquivo (%u) /tmp/datafile foi aberto com sucesso!", fd);
    
    ftruncate(fd, getpagesize());
    mem=mmap(NULL, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if(mem == MAP_FAILED)
    {
        perror("Não pôde mapear o endereço de memória do arquivo!");
        return FAILED;
    }
    fprintf(stdout, "\n==> Debug:\tMapeamento virtual foi criado com sucesso!");//*/
    fprintf(stdout, "\n==> Debug:\tMemória virtual: %ls ", mem);


    for (idx=0; idx<COUNT_FOR; idx++)
    {
        data.index = idx;
        data.value = rand();

        if (lseek (fd, 0, SEEK_SET) == -1)
        {
            fprintf(stdout, "\n==> Debug:\tFalha no lseek!");
            return FAILED;
        }
        
        write(fd, &data, sizeof(TData));
        fprintf(stdout, "==> Debug:\tWriter escreveu: [indice, valor] [%d, %d]\n", data.index, data.value);
        fprintf(stdout, "==> Debug:\tWriter entra em Sleep por %d segundos.\n", SLEEP_TIME);
        
        sleep(SLEEP_TIME);
    }
    //
    munmap(mem, getpagesize());

    // Fecha o arquivo
    close(fd);

    fprintf(stdout, "\n==> Debug:\tFinalizando o software Writer.\n");
    return SUCCESS;
}