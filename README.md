# Memory Mappings - Example 1

Esse projeto mostra dois softwares: o Escritor (writer) e o Leitor (reader), representando dois processos diferentes.

## Os softwares

### Writer

O Escritor (writer) abre um arquivo para escrever nele uma estrutura definida no arquivo tipos.h, e cria um mapeamento no Virtual Memory Space, compartilhando com outros processos.

Ele escreverá a cada 30 segundos um valor randômico e um índice, que vai de 0.
 a 99.

### Reader

O Leitor (Reader) abre um arquivo e cria um mapeamento para o mesmo arquivo que foi aberto pelo Escritor (Writer). O ponteiro recebido fará referência a informação armazenada no arquivo pelo Escritor (Writer).

Dessa forma, o que o Escritor escrever no arquivo, o Leitor terá acesso a mesma informação.

## Como compilar?

Para compilar o Escritor (writer), basta entrar na pasta writer, e executar o comando:

```bash
make all
```

Para compilar o Leitor (Reader), basta entrar na pasta reader, e executar o seguinte comando:

```bash
make all
```

## O Teste

Para fazer o teste, abra dois prompt de comando dentro da pasta do projeto. Em seguida, execute o write e o reader, um em cada prompt de comando.

### Comandos para executar:

Reader:

```bash
./reader
```

Writer:


```bash
./writer
```

Tiago Sousa Rocha
[Email](mailto:tsrrocha@gmail.com)
[LinkedIn](https://www.linkedin.com/in/tsrrocha/)